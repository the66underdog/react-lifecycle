import React from "react";
import './styles.css';
import { SIGNS as values } from './constants';
import PropTypes from 'prop-types';

class Calc extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            output: props.value,
            company: props.name,
        };
    }

    changeValue(value) {
        if (this.state.output === '0') {
            this.setState({ output: '' },
                () => {
                    this.setState({
                        output: this.state.output + value
                    });
                });
        }
        else {
            this.setState({
                output: this.state.output + value
            });
        }
    }

    opsSolver(el) {
        if (el === '+' || el === '-') {
            this.setState({ output: this.state.output + el });
        }
        if (el === 'reset') {
            this.setState({ output: '0' });
        }
        if (el === '=') {
            try {
                this.setState({ output: calc(this.state.output) });
            }
            catch {
                this.setState({ output: ':ERROR:' });
                setTimeout(() => { this.setState({ output: '0' }) }, 2500);
            }
        }

        function calc(expr) {
            return new Function('return ' + expr)();
        }
    }

    render() {
        return (
            <div className="main-container">
                <div className='calc'>
                    <div className='margin-controller'>
                        <input type="text" value={this.state.output} readOnly />
                        <h2 className='model'>{this.state.company}</h2>
                        <div className='flex-controller'>
                            <div className='buttons-numbers'>
                                {values.numbers.map((el, index) => { return <button key={index.toString()} onClick={() => this.changeValue(el)}>{el}</button> })}
                            </div>
                            <div className='buttons-ops'>
                                {values.ops.map((el, index) => { return <button key={index.toString()} onClick={() => { this.opsSolver(el) }}>{el}</button> })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Calc.propTypes = {
    value: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
}

export default Calc;